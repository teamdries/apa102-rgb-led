#include <stdio.h>
#include <stdint.h>
#include <wiringPi.h>
#include <wiringPiSPI.h>
#include <string.h>

void change_color_leds(uint8_t blue,uint8_t green, uint8_t red);
void file_to_led(void);
int main (int argc, char *argv[])
{

if (argc >  0) {
if(!strcmp(argv[1], "warm")) 
change_color_leds(35,165,0xff);

if(!strcmp(argv[1], "file"))
file_to_led(); 

if (argc > 2) 
change_color_leds(atoi(argv[1]), atoi(argv[2]), atoi(argv[3]));
}

else {
printf("File/Warm/ b g r"); 
}

return 0;
}



void change_color_leds(uint8_t blue,uint8_t green, uint8_t red) {
wiringPiSetup () ;

uint8_t pData[28]=
{0x00,0x00,0x00,0x00,
0xff,blue,green,red,
0xff,blue,green,red, 
0xff,blue,green,red, 
0xff,blue,green,red, 
0xff,blue,green,red,  
0xff,0x00,0x00,0x00};


if(wiringPiSPISetup (0, 0xF4240)==-1) // Channel: 0,Speed: 1MHz 
{
printf("could not initialise SPI\n");
}
else{
wiringPiSPIDataRW (0,pData,sizeof(pData)) ; // Channel: 0, pData, Size
}

}


void file_to_led(void) {



char fileblue;
char filegreen;
char filered;

FILE* fp;
char buffer[255];

fp = fopen("color.txt", "r");
int i = 0;
while(fgets(buffer, 255, (FILE*) fp)) {
    printf("%s", buffer);

if ( i == 0 )
fileblue = atoi(buffer);

if ( i == 1 )
filegreen = atoi(buffer);

if ( i == 2 )
filered = atoi(buffer);


i++;
}

change_color_leds(fileblue,filegreen,filered);

fclose(fp);


}

