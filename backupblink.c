#include <stdio.h>
#include <stdint.h>
#include <wiringPi.h>
#include <wiringPiSPI.h>


int main (void)
{
wiringPiSetup () ;
int chan=0;
int speed=0xF4240; //1MHz

uint8_t pData[1][28]={
									{0x00,0x00,0x00,0x00,
									0xE3,76,183,255, //orange ish color 
									0xE3,76,183,255, //
									0xE3,76,190,255, //
									0xE3,76,183,255, //
									0xE3,76,183,255, //
									0xE3,0x00,0x00,0x00}};


if(wiringPiSPISetup (chan, speed)==-1)
{
printf("could not initialise SPI\n");
}
else{
printf("Succes");
wiringPiSPIDataRW (chan,pData[0],sizeof(pData[0])) ;
}
	
return 0;
}
